# Copyright (c) 2005-2020, Matteo Cacciari, Gavin P. Salam and Gregory Soyez
#
#----------------------------------------------------------------------
# This file is part of FastJet.
#
#  FastJet is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  The algorithms that underlie FastJet have required considerable
#  development. They are described in the original FastJet paper,
#  hep-ph/0512210 and in the manual, arXiv:1111.6097. If you use
#  FastJet as part of work towards a scientific publication, please
#  quote the version you use and include a citation to the manual and
#  optionally also to hep-ph/0512210.
#
#  FastJet is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FastJet. If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

from __future__ import print_function

import os
import shutil
import subprocess

from setuptools import setup, find_packages
from setuptools.extension import Extension

# function to query a config binary and get the result
fastjet_config = os.environ.get('FASTJET_CONFIG', 'fastjet-config')
def query_config(query):
    return subprocess.check_output([fastjet_config, query]).decode('utf-8').strip()

# get fastjet info
fj_prefix = query_config('--prefix')
fj_cxxflags = query_config('--cxxflags')
fj_ldflags = query_config('--libs')
fj_version = query_config('--version')
if fj_version.endswith('-devel'):
  fj_version = fj_version.replace('-devel', 'a0')

# get cxxflags from environment, add fastjet cxxflags, and SWIG type table info
cxxflags = os.environ.get('CXXFLAGS', '').split() + fj_cxxflags.split() 
cxxflags += ['-DSWIG_TYPE_TABLE=fastjet', '-DSWIG', '-DHAVE_CONFIG_H', '-g0']

# determine library paths and names for Python
fj_libdirs, libs, ldflags = [], [], []
for x in fj_ldflags.split():
    if x.startswith('-L'):
        fj_libdirs.append(x[2:])
    elif x.startswith('-l'):
        libs.append(x[2:])
    else:
        ldflags.append(x)

pyinterface = os.path.dirname(__file__)
module = Extension('fastjet._fastjet',
                   sources=[os.path.join(pyinterface, 'swig_wrap.cpp')],
                   language='c++',
                   include_dirs=[os.path.join(pyinterface, '..', 'include')],
                   library_dirs=fj_libdirs,
                   libraries=libs,
                   extra_compile_args=cxxflags,
                   extra_link_args=ldflags)

# copy fastjet.py into fastjet/ subdirectory
#print('Copying pyinterface/fastjet.py into pyinterface/fastjet/')
#shutil.copy(os.path.join(pyinterface, 'fastjet.py'), os.path.join(pyinterface, 'fastjet'))

# run setup script
setup(
    version=fj_version,
    ext_modules=[module],
)
