#!/bin/bash

# install boost
curl -LO https://dl.bintray.com/boostorg/release/1.75.0/source/boost_1_75_0.tar.gz
tar xf boost_1_75_0.tar.gz
rm -f boost_1_75_0.tar.gz
cd boost_1_75_0
./bootstrap.sh --prefix=/usr/local --with-libraries=math
./b2 install
cd ..
rm -rf boost_1_75_0

# install CGAL
yum install -y lzma gmp-devel mpfr-devel
curl -LO https://github.com/CGAL/cgal/releases/download/v5.2/CGAL-5.2-library.tar.xz
tar xf CGAL-5.2-library.tar.xz
rm -f CGAL-5.2-library.tar.xz
cd CGAL-5.2
/opt/python/cp38-cp38/bin/pip install cmake
/opt/python/cp38-cp38/bin/cmake -DCMAKE_BUILD_TYPE=Release .
make install
cd ..
rm -rf CGAL-5.2

# download fastjet
if [ -z $1 ]; then
  curl -LO http://fastjet.fr/repo/fastjet-3.3.4.tar.gz
  tar xf fastjet-3.3.4.tar.gz
  rm -f fastjet-3.3.4.tar.gz
  mv fastjet-3.3.4 fastjet
  cd fastjet
else
  git clone https://gitlab.com/pkomiske/fastjet.git
  cd fastjet
  git submodule init
  git submodule update
  autoreconf -i
fi

# install fastjet
./configure --prefix=/usr/local --enable-pyext --enable-cgal --enable-cgal-header-only --disable-allplugins --disable-monolithic --disable-debug PYTHON=/opt/python/cp38-cp38/bin/python3 PYTHON_CONFIG=/opt/python/cp38-cp38/bin/python3-config
make -j4 install
cd ..
rm -rf fastjet