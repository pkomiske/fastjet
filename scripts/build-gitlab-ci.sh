#!/bin/bash

export CIBW_BUILD="cp*-manylinux_x86_64"
export CIBW_MANYLINUX_X86_64_IMAGE=registry.gitlab.com/pkomiske/fastjet:latest
export TWINE_USERNAME=__token__

pip3 install cibuildwheel twine
cibuildwheel --output-dir wheelhouse pyinterface
twine upload -r testpypi --skip-existing wheelhouse/*.whl
